package com.puc.controlepeso;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UserInsertActivity extends AppCompatActivity {

    private Button btnConfirm;
    private BDController bdController;
    private EditText etxtCintura, etxtAbdomem, etxtBraco, etxtPerna, etxtPeso;
    private Button btnDate;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_insert_new);

        bdController = new BDController(this);

        etxtCintura = (EditText) findViewById(R.id.etxtCintura);
        etxtAbdomem = (EditText) findViewById(R.id.etxtAbdomem);
        etxtBraco = (EditText) findViewById(R.id.etxtBraco);
        etxtPerna = (EditText) findViewById(R.id.etxtPerna);
        etxtPeso = (EditText) findViewById(R.id.etxtPeso);
        btnDate = (Button) findViewById(R.id.btn_date);

        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = dateFormat.format(cal.getTime());
        btnDate.setText(date);

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                btnDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float metricCintura = Float.parseFloat(etxtCintura.getText().toString());
                float metricAbdomem = Float.parseFloat(etxtAbdomem.getText().toString());
                float metricBraco = Float.parseFloat(etxtBraco.getText().toString());
                float metricPerna = Float.parseFloat(etxtPerna.getText().toString());
                float metricPeso = Float.parseFloat(etxtPeso.getText().toString());
                String date = btnDate.getText().toString().substring(0, 5);

                ParseObject tableMedidas = new ParseObject("Medidas");
                tableMedidas.put("metricBraco", metricBraco);
                tableMedidas.put("metricPerna", metricPerna);
                tableMedidas.put("metricAbdomem", metricAbdomem);
                tableMedidas.put("metricCintura", metricCintura);
                tableMedidas.put("metricPeso", metricPeso);
                //tableMedidas.put("metricDate", date);
                tableMedidas.saveInBackground();

                String msg = "Registro inserido";
                NavUtils.navigateUpFromSameTask(UserInsertActivity.this);
                Toast.makeText(UserInsertActivity.this, msg, Toast.LENGTH_SHORT).show();

                /*int resp = bdController.insereDado(metricCintura, metricAbdomem, metricBraco, metricPerna, metricPeso, date);

                String msg = "";

                if (resp != -1) {
                    msg = "Registro inserido";
                    NavUtils.navigateUpFromSameTask(UserInsertActivity.this);
                } else {
                    msg = "Falha no inserção do registro";
                }
                Toast.makeText(UserInsertActivity.this, msg, Toast.LENGTH_SHORT).show();*/
            }
        });
    }
}
