package com.puc.controlepeso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Rafael on 14/11/2015.
 */
public class ListMetricsAdapter extends ArrayAdapter<Metrics> {

    public ListMetricsAdapter(Context context, int resource, List<Metrics> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Pega a medida para aquela posicao
        Metrics metrics = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_metrics, parent, false);
        }

        // Recupera os elementos da tela
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtPeso = (TextView) convertView.findViewById(R.id.txtPeso);
        TextView txtBraco = (TextView) convertView.findViewById(R.id.txtBraco);
        TextView txtPerna = (TextView) convertView.findViewById(R.id.txtPerna);
        TextView txtAbdomem = (TextView) convertView.findViewById(R.id.txtAbdomem);
        TextView txtCintura = (TextView) convertView.findViewById(R.id.txtCintura);

        // Define os valores nos elementos
        txtPeso.setText("" + metrics.getPeso());
        txtBraco.setText("" + metrics.getBraco());
        txtPerna.setText("" + metrics.getPerna());
        txtAbdomem.setText("" + metrics.getAbdomem());
        txtCintura.setText("" + metrics.getCintura());

        // Converte e define a data
        if (metrics.getDate() != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM");
            txtDate.setText("" + dateFormat.format(metrics.getDate()));
        }

        return convertView;
    }
}
