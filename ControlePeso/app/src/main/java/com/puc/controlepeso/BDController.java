package com.puc.controlepeso;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Rafael on 12/11/2015.
 */
public class BDController {

    private SQLiteDatabase db;
    private BD banco;

    public BDController(Context context) {
        banco = new BD(context);
    }

    public int insereDado(double metricCintura, double metricAbdomem, double metricBraco, double metricPerna, double metricPeso, String date) {
        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(BD.METRIC_BRACO, metricBraco);
        valores.put(BD.METRIC_PERNA, metricPerna);
        valores.put(BD.METRIC_CINTURA, metricCintura);
        valores.put(BD.METRIC_ABDOMEM, metricAbdomem);
        valores.put(BD.METRIC_PESO, metricPeso);
        valores.put(BD.DATE, date);
        resultado = db.insert(BD.TABELA, null, valores);
        db.close();

        return (int) resultado;
    }

    public Cursor carregaDados(){
        Cursor cursor;
        String[] campos = {banco.ID, banco.METRIC_CINTURA, banco.METRIC_ABDOMEM, banco.METRIC_BRACO,banco.METRIC_PERNA, banco.METRIC_PESO, banco.DATE};

        db = banco.getReadableDatabase();
        cursor = db.query(banco.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null) {
            cursor.moveToFirst();
        }

        db.close();
        return cursor;
    }

    public void deletaDados() {
        db = banco.getWritableDatabase();
        db.execSQL("delete from "+ banco.TABELA);
        db.close();
    }

}
