package com.puc.controlepeso;

import java.util.Date;

public class Metrics {

	public Date date;
	public double peso, braco, cintura, perna, abdomem;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getBraco() {
        return braco;
    }

    public void setBraco(double braco) {
        this.braco = braco;
    }

    public double getCintura() {
        return cintura;
    }

    public void setCintura(double cintura) {
        this.cintura = cintura;
    }

    public double getPerna() {
        return perna;
    }

    public void setPerna(double perna) {
        this.perna = perna;
    }

    public double getAbdomem() {
        return abdomem;
    }

    public void setAbdomem(double abdomem) {
        this.abdomem = abdomem;
    }
}
