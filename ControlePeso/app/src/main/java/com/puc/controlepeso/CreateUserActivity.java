package com.puc.controlepeso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class CreateUserActivity extends AppCompatActivity {

    private EditText etxtUsuario, etxtEmail, etxtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        Button btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
        etxtUsuario = (EditText) findViewById(R.id.etxtUsuario);
        etxtEmail = (EditText) findViewById(R.id.etxtEmail);
        etxtSenha = (EditText) findViewById(R.id.etxtSenha);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = new ParseUser();
                user.setUsername(etxtUsuario.getText().toString());
                user.setPassword(etxtSenha.getText().toString());
                user.setEmail(etxtEmail.getText().toString());

                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            Intent intent = new Intent(CreateUserActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(CreateUserActivity.this, "Problema no cadastro", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}
