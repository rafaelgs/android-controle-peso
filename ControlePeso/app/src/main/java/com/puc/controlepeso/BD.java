package com.puc.controlepeso;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper {

    public static final String NOME_BANCO = "bd.db";
    public static final String TABELA = "Metrics";
    public static final String ID = "_id";
    public static final String METRIC_BRACO = "braco";
    public static final String METRIC_PERNA = "perna";
    public static final String METRIC_CINTURA = "cintura";
    public static final String METRIC_ABDOMEM = "abdomem";
    public static final String METRIC_PESO = "peso";
    public static final String DATE = "data";
    public static final int VERSAO = 1;

    public BD(Context context){
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+TABELA+" ( " + ID + " integer primary key autoincrement," + METRIC_BRACO + " double," + METRIC_PERNA + " double," + METRIC_CINTURA + " double," + METRIC_ABDOMEM + " double," + METRIC_PESO + " double," + DATE + " text" +")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA);
        onCreate(db);
    }
}
